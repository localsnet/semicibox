def gitUrl = 'http://gitlab.mydev.org/ich/app1-servlet'
def gitUrlSelenium = 'http://gitlab.mydev.org/ich/Selenium'



makeCiJob("app1-app", gitUrl, "app/pom.xml")
makeSonarJob("app1-app", gitUrl, "app/pom.xml")
makeDockerBuildJob("app1-app", "app")
makeDockerStartJob("app1-app", "app", "48080")
makeSeleniumJob("app1-app", "app", gitUrlSelenium, "pom.xml")
// makeDockerPushJob("app1-app", "app")
makeAnsibleDeployJob("app1-app", "app")


def makeCiJob(def jobName, def gitUrl, def pomFile) {
    
  println "############################################################################################################"
  println "Making CI Maven-Nexus Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-1-ci") {
    parameters {
      stringParam("BRANCH", "master", "Define TAG or BRANCH to build from")
      stringParam("REPOSITORY_URL", "http://nexus:8081/repository/maven-releases/", "Nexus Release Repository URL")
    }
    scm {
      git {
        remote {
          url(gitUrl)
        }
        extensions {
          cleanAfterCheckout()
        }
      }
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    triggers {
      scm('30/H * * * *')
      githubPush()
    }
    steps {
      maven {
          goals('clean versions:set -DnewVersion=DEV-\${BUILD_NUMBER}')
          mavenInstallation('Maven 3.3.3')
          rootPOM( pomFile )
          mavenOpts('-Xms512m -Xmx1024m')
          providedGlobalSettings('bj40ebe0-68e2-4fa3-ab30-86592113a63c')
      }
      maven {
        goals('clean deploy')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bj40ebe0-68e2-4fa3-ab30-86592113a63c')
      }
    }
    publishers {
      archiveXUnit {
        jUnit {
          pattern('**/target/surefire-reports/*.xml')
          skipNoTestFiles(true)
          stopProcessingIfError(true)
        }
      }
      publishCloneWorkspace('**', '', 'Any', 'TAR', true, null)
      downstreamParameterized {
        trigger("${jobName}-2-sonar") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def makeSonarJob(def jobName, def gitUrl, def pomFile) {
    
  println "############################################################################################################"
  println "Making Sonar Testing Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-2-sonar") {
    parameters {
      stringParam("BRANCH", "master", "Define TAG or BRANCH to build from")
    }
    scm {
      cloneWorkspace("${jobName}-1-ci")
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    steps {
      maven {
        goals('org.jacoco:jacoco-maven-plugin:0.7.4.201502262128:prepare-agent install -Psonar')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bj40ebe0-68e2-4fa3-ab30-86592113a63c')
      }
      maven {
        goals('sonar:sonar -Psonar')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bj40ebe0-68e2-4fa3-ab30-86592113a63c')
      }
    }
    publishers {
      downstreamParameterized {
        trigger("${jobName}-3-docker-build") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def makeDockerBuildJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Making Docker Build Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-3-docker-build") {
    logRotator {
        numToKeep(10)
    }
    scm {
      cloneWorkspace("${jobName}-1-ci")
    }
    steps {
      steps {
        shell("cd ${folder} && sudo /usr/bin/docker build -t app1-${folder} .")
      }
    }
    publishers {
      downstreamParameterized {
        trigger("${jobName}-4-docker-start-container") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def makeDockerStartJob(def jobName, def folder, def port) {

  println "############################################################################################################"
  println "Making Docker Start Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-4-docker-start-container") {
    logRotator {
        numToKeep(10)
    }
    steps {
      steps {
        shell('echo "Stopping Docker Container first"')
        shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") | true ")
        shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") | true ")
        shell('echo "Starting Docker Container"')
        shell("sudo /usr/bin/docker run -d --name app1-${folder} -p=${port}:8080 --network=ci_mynetwork app1-${folder}")
      }
    }
   publishers {
downstreamParameterized {
        trigger("${jobName}-5-selenium") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def makeSeleniumJob(def jobName, def folder, def gitUrlSelenium, def pomFile) {
    
  println "############################################################################################################"
  println "Making Selenium Testing Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-5-selenium") {
    parameters {
      choiceParam('app_host', ['http://172.26.24.141:48080/app1', 'https://www.google.com'])
      choiceParam('browser_host', ['http://firefox:4444/wd/hub', 'http://chrome:4444/wd/hub'])
     
    }
    scm {
      git {
        remote {
          url(gitUrlSelenium)
        }
        extensions {
          cleanAfterCheckout()
        }
      }
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    triggers {
      scm('30/H * * * *')
      githubPush()
    }
    steps {
      shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=firefox\") |true")
      shell("sudo /usr/bin/docker run --rm -d -p 4444:4444 --name=firefox --network=cidemo_mynetwork selenium/standalone-firefox")
      maven {
        goals('clean test')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bj40ebe0-68e2-4fa3-ab30-86592113a63c')
      }
      
    }
    steps {
       shell("sudo /usr/bin/docker stop firefox")
       shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") |true")
       shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") |true")
    }
    publishers {
      archiveJunit('**/target/surefire-reports/*.xml')
      downstreamParameterized {
        trigger("${jobName}-6-ansible-push-and-deploy") {
          parameters {
            currentBuild()
          }
        }
      }
      
    }
  }
}



/**def makeDockerPushJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Making Docker Push Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-6-docker-push") {
    logRotator {
        numToKeep(10)
    }
    steps {
      steps {
        shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") |true")
        shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=app1-${folder}\") |true")
        shell("sudo /usr/bin/docker login -u admin -p admin123 nexus:8083")
        shell("sudo /usr/bin/docker tag app1-app:latest nexus:8083/app1-app:1")
        shell("sudo /usr/bin/docker push nexus:8083/app1-app:1")
      }
    }
 publishers {
downstreamParameterized {
        trigger("${jobName}-7-ansible-deploy") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}
*/

def makeAnsibleDeployJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Ansible Deploying Docker Container Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-6-ansible-push-and-deploy") {
    logRotator {
        numToKeep(10)
    }
      steps {
          ansibleTower()
      }

  }


buildPipelineView('Pipeline') {
    filterBuildQueue()
    filterExecutors()
    title('app1 App CI Pipeline')
    displayedBuilds(5)
    selectedJob("app1-app-1-ci")
    alwaysAllowManualTrigger()
    refreshFrequency(60)
}

listView('app1 App') {
    description('')
    filterBuildQueue()
    filterExecutors()
    jobs {
        regex(/app1-app-.*/)
    }
    columns {
        status()
        buildButton()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
    }
}
}
